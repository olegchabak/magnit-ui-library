export default {
	components: true,
	buildModules: [
		// подключение плагина для глобальных переменных и миксинов scss
		'@nuxtjs/style-resources',
		// подключение своих компонентов из папки src/components
		'magnit-ui-library/nuxt',
	],
	// это подключение глобальных переменных и миксинов scss. Не использовать для подключения самих стилей!
	// https://github.com/nuxt-community/style-resources-module
	styleResources: {
		scss: [
			'../src/assets/scss/variables.scss',
			'../src/assets/scss/mixins.scss',
		]
	},
}
