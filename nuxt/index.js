// благодаря этому файлу происходит глобальное подключение компонентов из папки src/components
import { join } from "path";

export default function () {
  const { nuxt } = this

  // Make sure components is enabled
  if (!nuxt.options.components) {
    throw new Error('Установите `components: true` внутри `nuxt.config` и убедитесь, что используется `nuxt >= 2.13.0`')
  }

  this.nuxt.hook("components:dirs", (dirs) => {
    // Add ./components dir to the list
    dirs.push({
      path: join(__dirname, "../src/components"),
      // необязательный параметр - добавляет префикс компонентам. Теперь вызов выглядит <ui-test-button />
      prefix: "ui",
    });
  });
}
